//
//  AppInfoWaitViewController.m
//  DectarDriver
//
//  Created by Aravind Natarajan on 27/04/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

#import "AppInfoWaitViewController.h"

@interface AppInfoWaitViewController ()

@end

@implementation AppInfoWaitViewController
@synthesize containerView,initialLoadingImageView,headerlbl,subheaderLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    containerView.layer.cornerRadius=5;
    containerView.layer.masksToBounds=YES;
    initialLoadingImageView.frame=self.view.frame;
    initialLoadingImageView.image= [UIImage imageNamed:@"InitialLoadingImage"];
    if([Themes hasAppDetails]==NO){
        initialLoadingImageView.hidden=NO;
    }else{
         initialLoadingImageView.hidden=YES;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveAppInfoNotification:)
                                                 name:@"NotifForAppInfo"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getLocationInitially:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedInitially
                                               object:nil];
    headerlbl.text = JJLocalizedString(@"Sorry_for_the_delay",nil);
    subheaderLbl.text = JJLocalizedString(@"we_had_some_problem_connecting",nil);
    // Do any additional setup after loading the view.
}
- (void)receiveAppInfoNotification:(NSNotification *) notification
{
    initialLoadingImageView.hidden=YES;
    CLLocation *location = [[JJLocationManager sharedManager] currentLocation];
    if(location==nil||location.coordinate.latitude==0){
        
    }else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    
}
-(void)getLocationInitially:(NSNotification*)notification
{
    CLLocation *location = [[JJLocationManager sharedManager] currentLocation];
   if(location!=nil||location.coordinate.latitude!=0){
       if([Themes hasAppDetails]){
            [self dismissViewControllerAnimated:NO completion:nil];
       }
   }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
