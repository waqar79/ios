//
//  ContactUs-ViewController.h
//  Dectar
//
//  Created by Anaum Hamid on 1/3/17.
//  Copyright © 2017 CasperonTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Themes.h"
#import <MessageUI/MessageUI.h> 
#import "REFrostedViewController.h"

@interface ContactUs_ViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *Call;
@property (strong, nonatomic) IBOutlet UIButton *email;
@property (strong, nonatomic) IBOutlet UIButton *whatsapp;
@property (strong, nonatomic) IBOutlet UIButton *linkedin;
@property (strong, nonatomic) IBOutlet UIButton *facebook;
@property (strong, nonatomic) IBOutlet UIButton *insta;
@property (strong, nonatomic) IBOutlet UIButton *twitter;
@property (strong, nonatomic) IBOutlet UIButton *msger;
@property (strong, nonatomic) IBOutlet UIButton *menu;


@property (strong, nonatomic) IBOutlet UIView *bg_view;
@property (strong, nonatomic) IBOutlet UIView *heading_view;


- (IBAction)call_action:(id)sender;
- (IBAction)Email_action:(id)sender;
- (IBAction)whatsapp_action:(id)sender;
- (IBAction)LinkedIn_action:(id)sender;
- (IBAction)facebook_action:(id)sender;
- (IBAction)twitter_action:(id)sender;
- (IBAction)msger_action:(id)sender;
- (IBAction)insta_action:(id)sender;
- (IBAction)didClickMenuBtn:(id)sender;


@end
