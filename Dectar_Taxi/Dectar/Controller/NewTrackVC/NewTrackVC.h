//
//  NewTrackVC.h
//  Dectar
//
//  Created by Aravind Natarajan on 12/30/15.
//  Copyright © 2015 CasperonTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "DriverRecord.h"
#import "RootBaseVC.h"
#import "FareRecord.h"

@interface NewTrackVC : RootBaseVC<UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *DriverName;
@property (strong, nonatomic) IBOutlet UILabel *CarModel;
@property (strong, nonatomic) IBOutlet UILabel *CarNumber;
@property CGFloat Driver_latitude;
@property CGFloat Driver_longitude;
@property CGFloat User_latitude;
@property CGFloat User_longitude;
@property (strong, nonatomic) IBOutlet UIView *MapBG;
@property (strong ,nonatomic) DriverRecord * TrackObj;
@property (strong ,nonatomic) NSString * Driver_MobileNumber;
@property (strong ,nonatomic) GMSMapView * GoogleMap;
@property (strong ,nonatomic) GMSCameraPosition * Camera;
@property (strong,nonatomic) NSString * Ride_ID;
@property (strong,nonatomic) NSString * Reason_Str;
@property (strong,nonatomic) NSString *Reason_ID;

@property (strong,nonatomic) NSString *dr_Lat;
@property (strong,nonatomic) NSString *dr_Lon;

@property (strong, nonatomic) IBOutlet UILabel *rating;

@property (strong, nonatomic) IBOutlet UIButton *Cancel_Ride;
@property (strong, nonatomic) IBOutlet UIImageView *Driver_image;
@property (strong, nonatomic) IBOutlet UIImageView * Cap_image;

@property (strong, nonatomic) IBOutlet UILabel *Title_lbl;
@property (strong, nonatomic) IBOutlet UIScrollView *parallax_Scroll;
@property (strong, nonatomic) FareRecord * objrecFar;
@property (strong, nonatomic) IBOutlet UIButton *PanicBtn;
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;
@property (strong, nonatomic) IBOutlet UIButton *done_btn;
@property (strong, nonatomic) IBOutlet UIButton *contact;

@property (strong, nonatomic) IBOutlet UIButton *cancelAA;
@property (strong, nonatomic) IBOutlet UIButton *cancelBB;
@property (strong, nonatomic) IBOutlet UIButton *cancelCC;

@property CGFloat Drop_latitude;
@property CGFloat Drop_longitude;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) IBOutlet UIView *bg_view;
@property (strong, nonatomic) IBOutlet UIView *heading_view;

@property (weak, nonatomic) IBOutlet UIView *AlertView;
@property (weak, nonatomic) IBOutlet UIView *CarDetailView;
@property (weak, nonatomic) IBOutlet UIView *DriverDetailView;

@property (strong, nonatomic) IBOutlet UIButton *DriverViewbtn;
@property (strong, nonatomic) IBOutlet UIButton *CarViewbtn;

@property (weak, nonatomic) IBOutlet UIView *CC;
@property (weak, nonatomic) IBOutlet UIView *BB;
@property (weak, nonatomic) IBOutlet UIView *AA;
-(IBAction)Car:(id)sender;
-(IBAction)Driver:(id)sender;

-(IBAction)Police:(id)sender;
-(IBAction)Ambulance:(id)sender;
-(IBAction)Fire:(id)sender;
-(IBAction)Cancel:(id)sender;


@end
