//
//  RateCardVC.h
//  Dectar
//
//  Created by Suresh J on 24/08/15.
//  Copyright (c) 2015 CasperonTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootBaseVC.h"


@interface RateCardVC : RootBaseVC
@property (strong, nonatomic) IBOutlet UILabel *First_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *After_lbl;
@property (strong, nonatomic) IBOutlet UILabel *headerlbl;
@property (strong, nonatomic) IBOutlet UILabel *standardLBl;

@property (strong, nonatomic) IBOutlet UILabel *extraLbl;
@property (weak, nonatomic) IBOutlet UILabel *firstDot;
@property (weak, nonatomic) IBOutlet UILabel *AfterDot;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectCarTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *movingPerKmLbl;
@property (weak, nonatomic) IBOutlet UILabel *waitingPerHourLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *startingLbl;
@property (weak, nonatomic) IBOutlet UILabel *laterLbl;
@property (weak, nonatomic) IBOutlet UILabel *movingPerKm;
@property (weak, nonatomic) IBOutlet UILabel *waitingPerHour;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;

@property (strong, nonatomic) IBOutlet UIView *bg_view;
@property (strong, nonatomic) IBOutlet UIView *heading_view;

@end
