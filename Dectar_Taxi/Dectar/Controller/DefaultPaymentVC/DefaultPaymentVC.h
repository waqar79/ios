//
//  DefaultPaymentVC.h
//  Dectar
//
//  Created by Macbook Pro on 11/3/16.
//  Copyright © 2016 CasperonTechnologies. All rights reserved.
//

#import "RootBaseVC.h"

@interface DefaultPaymentVC : RootBaseVC

- (IBAction)creditFirstAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *useCreditFirst;
@property (weak, nonatomic) NSString *useCredit;
@property (strong, nonatomic) IBOutlet UIView *bg_view;
@property (strong, nonatomic) IBOutlet UIView *heading_view;

@end
