//
//  DefaultPaymentVC.m
//  Dectar
//
//  Created by Macbook Pro on 11/3/16.
//  Copyright © 2016 CasperonTechnologies. All rights reserved.
//

#import "DefaultPaymentVC.h"
#import "MyRideCell.h"
#import "Themes.h"
#import "UrlHandler.h"
#import "MyRideRecord.h"
#import "DetailMyRideVc.h"
#import "Constant.h"
#import "REFrostedViewController.h"
#import "LanguageHandler.h"
#import "HMSegmentedControl.h"

@interface DefaultPaymentVC ()

@end

@implementation DefaultPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_bg_view setBackgroundColor:[UIColor whiteColor]];
    [_heading_view setBackgroundColor:[UIColor whiteColor]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    [self navigationBarCreate];
    _useCredit = [Themes useCreditFirst];
    if ([_useCredit isEqualToString:@"1"]) {
        [_useCreditFirst setOn:YES animated:YES];
    }
    else{
        [_useCreditFirst setOn:NO animated:NO];
    }
    
}
- (IBAction)didClickMenuBtn:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
}

- (void)navigationBarCreate {
    self.navigationItem.title = @"DEFAULT PAYMENT MODE";
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor],
                                                                    NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0]};
    
//    UIBarButtonItem  * saveButton =[[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(btnDone:)];
//    
//    
//
//        [[self navigationController] setNavigationBarHidden:NO animated:NO];
//        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"who-is-where-icon"]];
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar_bg"] forBarMetrics:UIBarMetricsDefault];
//        self.navigationItem.hidesBackButton = YES;
//        UIBarButtonItem * btnLeft = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"w-icon"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
//        self.navigationItem.leftBarButtonItem = btnLeft;
//        //    UIBarButtonItem * btnRight = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconEdit"] style:UIBarButtonItemStyleDone target:self action:NULL];
//        //    self.navigationItem.rightBarButtonItem = btnRight;
   
}




- (IBAction)creditFirstAction:(id)sender {
    if([sender isOn]){
        NSLog(@"Switch is ON");
        [self saveData:[NSNumber numberWithInteger:1]];
        
    } else{
        NSLog(@"Switch is OFF");
        [self saveData:[NSNumber numberWithInteger:0]];
        
    }

}

-(void)saveData:(NSNumber *)credit_bit
{
    
    NSDictionary *parameters=@{@"user_id":[Themes getUserID],
                               @"use_credit":credit_bit
                              };
    
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [Themes StartView:self.view];
    [web UseCreditFirst:parameters success:^(NSMutableDictionary *responseDictionary)
     {
         [Themes StopView:self.view];
         
         if ([responseDictionary count]>0)
         {
             NSLog(@"%@",responseDictionary);
             responseDictionary=[Themes writableValue:responseDictionary];
             NSString * comfiramtion=[responseDictionary valueForKey:@"response"];
             NSString *creditType=[responseDictionary valueForKey:@"use_credit"];
             [Themes useCreditFirst:creditType];
             [Themes StopView:self.view];
//             if ([comfiramtion isEqualToString:@"1"])
//             {
//                 
                 UIAlertView*Alert=[[UIAlertView alloc]initWithTitle:@"Success\xF0\x9F\x91\x8D" message:comfiramtion delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [Alert show];
//              //   [saveBtn setHidden:NO];
//                 
//                 [self.navigationController popViewControllerAnimated:YES];
//             }
//             else
//             {
//                 NSString *titleStr = JJLocalizedString(@"Oops", nil);
//                 UIAlertView*Alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@\xF0\x9F\x9A\xAB",titleStr] message:alert delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                 [Alert show];
//             }
             
         }
         
     }
               failure:^(NSError *error)
     {
         [Themes StopView:self.view];
     }];
    
}


@end

