//
//  ContactUs-ViewController.m
//  Dectar
//
//  Created by Anaum Hamid on 1/3/17.
//  Copyright © 2017 CasperonTechnologies. All rights reserved.
//

#import "ContactUs-ViewController.h"

@interface ContactUs_ViewController ()

@end

@implementation ContactUs_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_bg_view setBackgroundColor:[UIColor whiteColor]];
    [_heading_view setBackgroundColor:[UIColor whiteColor]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)call_action:(id)sender{
    NSString * mystring = [Themes GetCustomerServiceNumber];
    NSLog(@"%@",mystring);
    NSArray * array = [mystring componentsSeparatedByString:@" "];
    NSString * str1 = [array objectAtIndex:0]; //123
    NSLog(@"%@",str1);
    NSString * str2 = [array objectAtIndex:1]; //4
    NSLog(@"%@",str2);
    //NSString *phNo = @"+919876543210";
    
   // NSString* actionStr = [NSString stringWithFormat:@"telprompt:%@",Driver_MobileNumber];
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionStr]];
    NSString *phoneUrl = [NSString  stringWithFormat:@"telprompt:%@",str1];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneUrl]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneUrl]];
    } else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }

}
- (IBAction)Email_action:(id)sender{
    [self sendEmail];
}
- (IBAction)whatsapp_action:(id)sender{
    NSLog(@"do nothing");
}
- (IBAction)LinkedIn_action:(id)sender{
    NSLog(@"do nothing");
}
- (IBAction)facebook_action:(id)sender{
    NSLog(@"do nothing");
    // Check if FB app installed on device
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/862528187225439"]];
    }
    else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/URIDEAE/"]];
    }
    

}
- (IBAction)twitter_action:(id)sender{
    NSLog(@"do nothing");
}
- (IBAction)msger_action:(id)sender{
    NSLog(@"do nothing");
}
- (IBAction)insta_action:(id)sender{
    NSLog(@"do nothing");
}

- (void)sendEmail {
    // Email Subject
    NSString *emailTitle = @"Uride Support";
    // Email Content
    NSString *messageBody = @"Uride Support";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@uride.ae"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didClickMenuBtn:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


@end
