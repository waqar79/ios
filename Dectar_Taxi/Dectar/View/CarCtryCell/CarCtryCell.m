//
//  CarCtryCell.m
//  Dectar
//
//  Created by Aravind Natarajan on 8/18/15.
//  Copyright (c) 2015 CasperonTechnologies. All rights reserved.
//

#import "CarCtryCell.h"
#import "UIImageView+Network.h"
#import "UIImageView+WebCache.h"


@implementation CarCtryCell
@synthesize objBookingRecord,selectiveIndexpath,carButton,TimeLable,nameLable,cabImageView;

-(IBAction)SelectButton:(id)sender
{
    
    [self.delegate buttonWasPressed:selectiveIndexpath];
}
-(NSArray *)carImages {
    return @[@"budget_grey_car", @"economy_grey_car", @"business_grey_car", @"mini_grey_car"];
}
-(NSArray *)selectedCarImages {
    return @[@"budget_active_car", @"economy_active_car", @"business_active_car", @"mini_active_car"];
}

-(void)setDatasToCategoryCell:(BookingRecord *)objBookingRec indexPath:(NSIndexPath *)indexPath{
   // cabImageView.layer.cornerRadius=carButton.frame.size.width/2;
   // cabImageView.layer.masksToBounds=YES;
    TimeLable.text= [objBookingRec.categoryETA isEqualToString:@"No cabs"] ? @"All engaged" : objBookingRec.categoryETA;
    nameLable.text=objBookingRec.categoryName;
    
    
    if(objBookingRec.isSelected==YES){
        cabImageView.image = [UIImage imageNamed:[self selectedCarImages][indexPath.row]];
        nameLable.textColor=TimeLable.textColor=[UIColor colorWithRed:239.0/255 green:148.0/255 blue:54.0/255 alpha:1.0];
        //        [cabImageView loadImageFromURL:[NSURL URLWithString:objBookingRec.Active_Image] placeholderImage:[UIImage imageNamed:@""] cachingKey:cacheStr];
       /* [cabImageView sd_setImageWithURL:[NSURL URLWithString:objBookingRec.Active_Image] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];*/
        
//        self.backgroundColor=[UIColor colorWithRed:239.0/255 green:148.0/255 blue:54.0/255 alpha:1.0];
//        [cabImageView sd_setImageWithURL:[NSURL URLWithString:objBookingRec.Active_Image] placeholderImage:[UIImage imageNamed:@"CabPlaceholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            
//        }];
        
        
    }else{
       cabImageView.image = [UIImage imageNamed:[self carImages][indexPath.row]];
        nameLable.textColor=TimeLable.textColor=[UIColor blackColor];
        //        [cabImageView loadImageFromURL:[NSURL URLWithString:objBookingRec.Normal_image] placeholderImage:[UIImage imageNamed:@""] cachingKey:cacheStr];
        
       /* [cabImageView sd_setImageWithURL:[NSURL URLWithString:objBookingRec.Normal_image] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];*/
//        self.backgroundColor=[UIColor whiteColor];
//        [cabImageView sd_setImageWithURL:[NSURL URLWithString:objBookingRec.Normal_image] placeholderImage:[UIImage imageNamed:@"CabPlaceholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            
//        }];
        
    }
    
    
}
@end