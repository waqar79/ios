//
//  BackgroundViewColor.m
//  Dectar
//
//  Created by Casperon Tech on 02/08/16.
//  Copyright © 2016 CasperonTechnologies. All rights reserved.
//

#import "BackgroundViewColor.h"

@implementation BackgroundViewColor

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseClassInit];
    }
    return self;
}

- (void)baseClassInit {
    
    //initialize all ivars and properties
    self.backgroundColor=BGCOLORVIEW;
}

@end
